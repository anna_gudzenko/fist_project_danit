import React from "react";
import "./button.scss";

class Button extends React.Component {
  render() {
    const { backgroundColor, text, id, openModal } = this.props;

    return (
      <div>
        <button
          id={id}
          className="btn"
          onClick={openModal}
          style={{ background: backgroundColor }}
        >
          {text}
        </button>
      </div>
    );
  }
}

export default Button;
