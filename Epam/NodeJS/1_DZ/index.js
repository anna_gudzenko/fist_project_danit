const express = require("express");
const app = express();
const fs = require("fs");
const path = require("path")

const port = 8080;

app.use(express.json());

let createFile=(req, res)=>{
    
    fs.writeFile(`${req.body.filename}`, `${req.body.content}`, (err)=>{
        
        if (err){
            res.status(500).json({ message: "Server error" })
        }else if(!req.body.content ){
            res.status(400).json({ message: "Please specify 'content' parameter" }) 
        }else if(!req.body.filename.match(/\.(log|txt|json|yaml|xml|js)$/i)){
            res.status(400).json({ message: "Extention 'filename' is not correct" }) 
        }else{
            res.status(200).json({ message: "File created successfully" })  
        }             
    })
}

let getFiles=(req, res)=>{
    const testFolder = './';
    fs.readdir(testFolder, (err, files) => {
        if (err){
            res.status(500).json({ message: "Server error" })
        }else if(!files){
            res.status(400).json({message: "Client error" }) 
        }else{
            res.status(200).json({message: "sucsess", "files": files})
        }
    });   
}

let getFile=(req, res)=>{
    let uploadDate; 
    fs.stat(`./${req.params.filename}`, (err, stats) => {
        uploadDate = stats.birthtime
    });

    fs.readFile(req.params.filename,'utf-8', (err, data) => {
        if (err){
            res.status(500).json({ message: "Server error" })
        }else if(!data){
            res.status(400).json({message: `No file with ${req.params.filename} filename found` }) 
        }else{
            res.status(200).json(
                {
                    message: "Sucsess", 
                    filename: req.params.filename,  
                    content: data, 
                    extension: path.extname(req.params.filename).substring(1),
                    uploadedDate: uploadDate
                })
            }
      });
}


app.post("/api/files", createFile)

app.get('/api/files', getFiles) 

app.get('/api/files/:filename', getFile);


app.listen(port, () => {
    console.log(`Server app listening on port ${port}`)
  })
