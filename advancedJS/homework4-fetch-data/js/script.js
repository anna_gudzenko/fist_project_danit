"use strict";

const URL = "https://ajax.test-danit.com/api/swapi/films";
const root = document.getElementById("root");

class StarWars {
  constructor(url, root) {
    this.url = url;
    this.root = root;
  }

  getInfo() {
    fetch(this.url)
      .then((response) => {
        response.json().then((data) => {
          this.render(data);
        });
      })
      .catch((e) => {
        console.log(e.message);
      });
  }

  render(array) {
    array.forEach((element) => {
      const ul = document.createElement("ul"),
        episodeItem = document.createElement("li"),
        titleItem = document.createElement("li"),
        contentItem = document.createElement("li");

      episodeItem.textContent = `Episode: ${element.episodeId}`;
      titleItem.innerHTML = `Title: ${element.name} <br/> Characters:`;
      contentItem.textContent = `Content: ${element.openingCrawl}`;
      ul.append(episodeItem);
      ul.append(titleItem);
      ul.append(contentItem);
      this.root.append(ul);

      const { characters } = element;
      this.getCaracters(characters, titleItem);
    });
  }

  getCaracters(personages, tagName) {
    const ul = document.createElement("ul");
    personages.map((personLink) => {
      fetch(personLink)
        .then((response) => {
          response.json().then((data) => {
            ul.innerHTML += `<li>${data.name}</li>`;
            return ul;
          });
        })
        .catch((e) => {
          console.log(e.message);
        });
    });
    tagName.append(ul);
  }
}
const film = new StarWars(URL, root).getInfo();
