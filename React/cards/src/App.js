import "./App.scss";
import React from "react";
import axios from "axios";
import CardsList from "./components/CardsList/CardsList";

class App extends React.Component {
  state = {
    modalOpen: false,
    goods: [],
    favoriteGoods: null,
    choseCard: null,
  };
  async componentDidMount() {
    const list = await axios("/api/goods.json");
    this.setState({
      goods: list.data,
      favoriteGoods: JSON.parse(localStorage.getItem("favorite")),
    });
    JSON.parse(localStorage.getItem("favorite"));
    if (!localStorage.getItem("cart") || !localStorage.getItem("favorite")) {
      localStorage.setItem("cart", JSON.stringify([]));
      localStorage.setItem("favorite", JSON.stringify([]));
    } else {
      const localArr = JSON.parse(localStorage.getItem("favorite"));
      list.data.forEach((cardEl) => {
        localArr.find((e) => cardEl.id === e.id && (cardEl.favorite = true));
        this.setState({ goods: list.data });
      });
    }
  }
  openModal = (card) => {
    this.setState({ modalOpen: true, choseCard: card });
  };
  closeModal = () => {
    this.setState({ modalOpen: false });
  };
  changeFavorite = (index) => {
    if (index) {
      let favorite = JSON.parse(localStorage.getItem("favorite"));
      const product = this.state.goods.find((item) => item.id === index);
      if (product.favorite === false) {
        favorite.push(product);
        product.favorite = true;
        localStorage.setItem("favorite", JSON.stringify(favorite));
        this.setState({ favoriteGoods: favorite });
      } else {
        favorite.splice(index - 1, 1);
        product.favorite = false;
        this.setState({ favoriteGoods: favorite });
        localStorage.removeItem("favorite");
        localStorage.setItem("favorite", JSON.stringify(favorite));
      }
    }
  };
  addToCart = () => {
    const cart = JSON.parse(localStorage.getItem("cart"));
    const product = this.state.goods.find(
      (item) => item.id === this.state.choseCard.id
    );
    cart.push(product);
    localStorage.setItem("cart", JSON.stringify(cart));
    this.closeModal();
  };
  render() {
    return (
      <div className="App">
        <CardsList
          goods={this.state.goods}
          openModal={this.openModal}
          closeModal={this.closeModal}
          modalOpen={this.state.modalOpen}
          changeFavorite={this.changeFavorite}
          addToCart={(id) => this.addToCart(id)}
        />
      </div>
    );
  }
}
export default App;
