"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, langs) {
    super(name, age, salary);
    this.langs = langs; //array
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value * 3;
  }
}

const programmerLena = new Programmer("Lena", 30, 1700, [
  "Eanglish",
  "Russian",
  "Ukrainian",
  "Polish",
]);
const programmerJonh = new Programmer("Jonh", 28, 1000, [
  "Eanglish",
  "Russian",
]);
const programmerVasyliy = new Programmer("Vasiliy", 32, 800, [
  "Ukrainian",
  "Russian",
]);
const programmerDavid = new Programmer("David", 27, 2500, [
  "Russian",
  "French",
  "Eanglish",
  "German",
]);
console.log(programmerLena);
console.log(programmerJonh);
console.log(programmerVasyliy);
console.log(programmerDavid);
