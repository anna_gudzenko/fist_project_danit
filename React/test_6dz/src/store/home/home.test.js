import reducer from "./reducer";
import actions from "./actions";
const initialState = {
  goods: {
    data: null,
    isLoading: true,
  },
  modalOpen: false,
  choseCard: null,
  favorite: false,
};

test("SET_GOODS_DATA action changes data prop in state", () => {
  const mockArr = [{ id: 2 }];
  const action = actions.setGoodsData(mockArr);
  const newState = reducer(initialState, action);
  expect(newState.goods.data).toBe(mockArr);
});

test("SET_GOODS_LOADING action change isLoading prop in state", () => {
  const action = actions.setGoodsLoading(false);
  const newState = reducer(initialState, action);
  expect(newState.goods.isLoading).toBeFalsy;
});

test("SET_MODAL_IS_OPEN action change modalOpen prop in state", () => {
  const action = actions.setModalIsOpen(true);
  const newState = reducer(initialState, action);
  expect(newState.modalOpen).toBeTruthy();
});

test("SET_CHOSEN_CARD action determinate cart in state", () => {
  const mockCard = { id: 1 };
  const action = actions.setChosenCard(mockCard);
  const newState = reducer(initialState, action);
  expect(newState.choseCard).toBe(mockCard);
});

test("CHANGE_FAVORITE_IN_CARD action change favorite prop in state", () => {
  const action = actions.changeFavoriteInCard(true);
  const newState = reducer(initialState, action);
  expect(newState.favorite).toBeTruthy();
});
