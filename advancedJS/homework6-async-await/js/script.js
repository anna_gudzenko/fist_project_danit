"use strict";
const URL = "https://api.ipify.org/?format=json";
const btn = document.getElementById("btn");

class IPInfo {
  constructor(button) {
    this.button = button;
  }
  async getIp() {
    const response = await fetch(URL);
    const ipObj = await response.json();
    const { ip } = ipObj;
    this.getAdressByIp(ip);
  }
  async getAdressByIp(ip) {
    const response = await fetch(
      "http://ip-api.com/json/" +
        ip +
        "?fields=status,continent,country,regionName,city,district"
    );
    const obj = await response.json();
    this.createAdress(obj);
  }
  createAdress(obj) {
    const ul = document.createElement("ul");
    ul.innerHTML = `<li>Kонтинент: ${obj.continent};</li> <li> Cтрана: ${obj.country}; </li> <li>Pегион: ${obj.regionName};</li> <li>Город: ${obj.city};</li> <li>Район города: ${obj.district}; </li>`;
    this.button.after(ul);
  }
}

btn.addEventListener("click", () => {
  new IPInfo(btn).getIp();
});
