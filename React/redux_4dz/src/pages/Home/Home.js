import React, { useEffect } from "react";
import Loader from "../../components/Loader/Loader";

import CardsList from "../../components/CardsList/CardsList";
import { connect } from "react-redux";
import operations from "../../store/operations";

function Home({ goods, isLoading, getGoods }) {
  useEffect(() => {
    getGoods();
  }, [getGoods]);
  if (isLoading) {
    return <Loader />;
  }
  return <CardsList goods={goods} deleteProduct={false} />;
}
const mapStateToProps = (state) => {
  return {
    goods: state.goods.data,
    isLoading: state.goods.isLoading,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getGoods: () => dispatch(operations.getGoods()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
