import React from "react";
import "./modal.scss";
import PropTypes from "prop-types";

class Modal extends React.Component {
  render() {
    const { closeModal, addToCart } = this.props;

    return (
      <div onClick={closeModal} className="modal-container">
        <div className="modal" onClick={(e) => e.stopPropagation()}>
          <header className="modal__header">
            <p className="modal__header-text">Добавить этот товар в корзину?</p>
            <button onClick={closeModal} className="modal__header-btn"></button>
          </header>
          <div className="modal__wrap">
            <p className="modal__wrap-text">
              При добавлении товара в корзину, Вы сможете изменить его
              количесто, и указать место доставки.
            </p>
            <div className="btn-wrap">
              <button onClick={addToCart} className="modal__btn">
                Добавить товар
              </button>
              <button className="modal__btn">Перейти в корзину</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
Modal.propTypes = {
  closeModal: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
};
export default Modal;
