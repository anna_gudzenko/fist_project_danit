import React, { useState } from "react";
import CardsList from "../../components/CardsList/CardsList";
import "./cart.scss";

function Cart(props) {
  // const [cart, setCart] = useState([]);
  // setCart(JSON.parse(localStorage.getItem("cart")));
  const [open, setopen] = useState(false);
  const [cardToDelete, setcardToDelete] = useState(null);
  const cartArr = JSON.parse(localStorage.getItem("cart"));

  const openDeleteModal = (card) => {
    setopen(!open);
    setcardToDelete(card);
  };
  const deleteFromCart = () => {
    if (cardToDelete) {
      if (cartArr) {
        const newCartArr = cartArr.filter((el) => el.id !== cardToDelete.id);
        localStorage.setItem("cart", JSON.stringify(newCartArr));
        console.log(newCartArr);
        openDeleteModal();
        if (newCartArr.length === 0) {
          localStorage.removeItem("cart");
        }
      }
    }
  };
  return (
    <>
      {cartArr ? (
        <div>
          <div className="cart-container">
            <div className="cart-img"></div>
            <span className="cart-count">{cartArr.length}</span>
          </div>
          <CardsList
            goods={cartArr}
            deleteProduct={true}
            openDeleteModal={openDeleteModal}
            open={open}
            deleteFromCart={deleteFromCart}
          />
        </div>
      ) : (
        <h2 className="empty-cart">Your cart is empty</h2>
      )}
    </>
  );
}

export default Cart;
