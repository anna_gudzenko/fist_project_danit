import reducer from "./reducer";
import actions from "./actions";
const initialState = {
  toggle: false,
};

test("CHANGE_SELECTED_TO_REMOVE action changes toggle prop in state", () => {
  const action = actions.changeSelectedToremove(true);
  const newState = reducer(initialState, action);
  expect(newState.toggle).toBe(true);
});
