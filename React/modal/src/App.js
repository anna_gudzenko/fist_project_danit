import "./App.scss";
import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
// const body = document.querySelector("body");
class App extends React.Component {
  state = {
    modalName: null,
    id: {
      firstId: "first",
      secondId: "second",
    },
    buttonFirst: {
      backgroundColor: "rgb(179, 56, 44)",
      text: "Open first modal",
    },
    buttonSecond: {
      backgroundColor: "rgb(38,147,91)",
      text: "Open second modal",
    },
    modalFirst: {
      header: "Do you want to delete this file?",
      closeButton: true,
      text: "Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?",
      action: (
        <div className="btn-wrap">
          <button className="modal__btn-first">Ok</button>
          <button className="modal__btn-first">Show More</button>
        </div>
      ),
    },
    modalSecond: {
      header: "Do you want to dounload this file?",
      closeButton: false,
      text: "If you can't download a file, the owner may have disabled options to print, download, or copy for people with only comment or view access.",
      action: (
        <div className="btn-wrap">
          <button className="modal__btn-second">Save</button>
          <button className="modal__btn-second">Cansel</button>
        </div>
      ),
    },
  };
  openModal = (evt) => {
    const { firstId, secondId } = this.state.id;
    if (evt.target.id === firstId) {
      this.setState({ modalName: firstId });
    }
    if (evt.target.id === secondId) {
      this.setState({ modalName: secondId });
    }
  };

  closeModal = () => {
    this.setState({ modalName: null });
  };
  render() {
    const { buttonFirst, buttonSecond, id, modalFirst, modalSecond } =
      this.state;
    return (
      <div className="App">
        <Button
          backgroundColor={buttonFirst.backgroundColor}
          text={buttonFirst.text}
          id={id.firstId}
          openModal={this.openModal}
        />
        <Button
          backgroundColor={buttonSecond.backgroundColor}
          text={buttonSecond.text}
          id={id.secondId}
          openModal={this.openModal}
        />
        {this.state.modalName && (
          <Modal
            headerText={
              this.state.modalName === "first"
                ? modalFirst.header
                : modalSecond.header
            }
            closeButton={
              this.state.modalName === "first"
                ? modalFirst.closeButton
                : modalSecond.closeButton
            }
            text={
              this.state.modalName === "first"
                ? modalFirst.text
                : modalSecond.text
            }
            action={
              this.state.modalName === "first"
                ? modalFirst.action
                : modalSecond.action
            }
            closeModal={this.closeModal}
          />
        )}
      </div>
    );
  }
}

export default App;
